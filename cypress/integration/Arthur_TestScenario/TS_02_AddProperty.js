/// <reference types="cypress" />

// declare global variables
let test_data
let login_object
let home_object
let property_object
let addproperty_object

context('Aliasing', () => {
  beforeEach(() => {
    // Given I am a user
    cy.fixture('Staging/test_data').then((variables) => {
        test_data = variables.ARTHUR
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        login_object = object.ARTHUR_LOGIN
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        home_object = object.ARTHUR_HOMEPAGE
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        property_object = object.ARTHUR_PROPERTY
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        addproperty_object = object.ARTHUR_ADD_PROPERTY
      })
  
    cy.visit('https://staging.arthuronline.co.uk/login?X-MODE=QA-eW91LXdpbi1ub3RoaW5nCg')
  })

  it('Add Property', () => {
    
    // And I am logged into the AUT already
    cy.get(login_object.Username)
      .type(test_data.Username, { delay: 10 })
      .should('have.value', test_data.Username)

    cy.get(login_object.Password)
      .type(test_data.Password, { delay: 10 })
      .should('have.value', test_data.Password)

    cy.get('.submit input').should('be.visible').click()
    cy.get('a .account-details').should('be.visible')
    
    // When I click on Properties dropdown on the left-hand panel
    cy.get(property_object.Properties).should('be.visible').click()
    cy.get(property_object.Title).should('be.visible').should('contain', 'Properties')

    // And I click on the Add property button on the property index page 
    cy.get(property_object.AddProperty).should('be.visible').click()
    cy.get(property_object.Title).should('be.visible').should('contain', 'Add Property and Units')

    // And I select Property with a multiple rentable units option
    cy.get(addproperty_object.PropertyWithMultiple).click()

    // And I enter all the required fields in the input text boxes
    cy.inputText(addproperty_object.ProptName,test_data.PropertyName)
    cy.inputText(addproperty_object.ProptAddress1,'test text Address 1')
    cy.inputText(addproperty_object.ProptAddress2,'test text Address number 2')
    cy.inputText(addproperty_object.ProptCity,'city is here')
    cy.inputText(addproperty_object.ProptCountry,'NowWhere country')
    cy.inputText(addproperty_object.ProptPostcode,'1ER42DF')

    cy.get(addproperty_object.ProptOwner).select('107876', { force: true })
    cy.get(addproperty_object.ProptArea).should('have.value', '').select('fag', { force: true })
    cy.get(addproperty_object.ProptCert).select('Other', { force: true })
 

    // And I make sure that the “I manage this Property” checkbox is ticked
    cy.get(addproperty_object.ProptManage).should('be.checked')

    // And I select the number of rentable units to be any number more than 1
    cy.inputText(addproperty_object.ProptUnit,'2')

    // And I click on the “Next, Units Settings” button
    cy.get(addproperty_object.NextUnit).should('be.visible').click({ force: true })
    cy.get(addproperty_object.SubTitle).should('be.visible').should('contain', 'Unit Settings')

    // And I select all fields under Unit Settings section
    cy.get(addproperty_object.UtSt_UnitType).eq(0).select('Barn', { force: true })
    cy.get(addproperty_object.UtSt_UnitOwner).eq(0).select('107876', { force: true })
    cy.get(addproperty_object.UtSt_Manager).eq(0).select('630435', { force: true })
    cy.get(addproperty_object.UtSt_Agent).eq(0).select('107929', { force: true })

    // cy.xpath('//table[@class=\'multi-unit-table\']//child::tr[2]//select[@id=\'MultiUnitIdUnitUnitTypeId\']')
    // .select('Barn', { force: true })
    cy.get(addproperty_object.UtSt_UnitType).eq(1).select('Barn', { force: true })
    cy.get(addproperty_object.UtSt_UnitOwner).eq(1).select('107876', { force: true })
    cy.get(addproperty_object.UtSt_Manager).eq(1).select('630435', { force: true })
    cy.get(addproperty_object.UtSt_Agent).eq(1).select('107929', { force: true })

    // And I click on Add Property button
    cy.get(addproperty_object.AddProperty).should('be.visible').click({ force: true })

    // Then I have successfully added a property with a multiple units 
    cy.get(addproperty_object.Title).should('be.visible').should('contain', 'Multiple Units Added')

    // And I re-click on Properties dropdown from the left-hand panel 
    cy.get(property_object.PropertiesSubLink).should('be.visible').eq(1).click()
    cy.get(property_object.Title).should('be.visible').should('contain', 'Properties')

    // And I validate that the property I have just created is visible on the property index page
    cy.inputText(property_object.SearchField,test_data.PropertyName)
    cy.get(property_object.SearchBtn).should('be.visible').click({ force: true })
    cy.get(property_object.ProptName,{ timeout: 10000 }).should('be.visible').should('contain',test_data.PropertyName)
    cy.get(property_object.ProptType).should('be.visible').should('contain','Residential')
  })

})
