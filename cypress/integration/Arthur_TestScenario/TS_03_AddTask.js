/// <reference types="cypress" />

// declare global variables
let test_data
let login_object
let home_object
let task_object
let addproperty_object

context('Aliasing', () => {
  beforeEach(() => {
    // Given I am a user
    cy.fixture('Staging/test_data').then((variables) => {
        test_data = variables.ARTHUR
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        login_object = object.ARTHUR_LOGIN
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        home_object = object.ARTHUR_HOMEPAGE
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        task_object = object.ARTHUR_TASK
      })
      cy.fixture('Staging/Object_Login').then((object) => {
        addproperty_object = object.ARTHUR_ADD_PROPERTY
      })
  
    cy.visit('https://staging.arthuronline.co.uk/login?X-MODE=QA-eW91LXdpbi1ub3RoaW5nCg')
  })

  it('Add task to property', () => {
    let RefNum
    
    // And I am logged into the AUT already
    cy.get(login_object.Username)
      .type(test_data.Username, { delay: 10 })
      .should('have.value', test_data.Username)

    cy.get(login_object.Password)
      .type(test_data.Password, { delay: 10 })
      .should('have.value', test_data.Password)

    cy.get('.submit input').should('be.visible').click()
    cy.get('a .account-details').should('be.visible')
    
    // And I have added a property already from the above scenario 2
    // When I click on Tasks dropdown on the left-hand panel to view the task index page
    cy.get(home_object.Task).scrollIntoView().should('be.visible').click()

    // And I click on Add Task button
    cy.get(task_object.AddTask).should('be.visible').click()
    
    // And I relate this task to the property I created from scenario 2 
    cy.get(task_object.SearchRelate).should('be.visible').click()
    cy.inputText(task_object.SearchRelateInput,test_data.PropertyName)
    cy.get(task_object.SearchRelateResult,{ timeout: 10000 }).contains(test_data.PropertyName).click()

    // And I enter all the required fields in the input text boxes on the add task page
    cy.inputText(task_object.Description,test_data.TaskDescription)
    cy.inputText(task_object.DateDue,'31/12/2021')
    cy.inputText(task_object.TimeDue,'18:38')
    cy.get(task_object.AssignTo).should('be.visible').click()
    cy.get(task_object.AssignToResult,{ timeout: 10000 }).contains('Cypress Tester 1').click()
    cy.get(task_object.TaskType).select('739684', { force: true })
  
    // And I click on Save Task button
    cy.get(task_object.SaveTask).should('be.visible').click()

    // Then I am redirected to the task view page of the newly created task
    cy.get(task_object.TaskViewPage).should('be.visible')

    // And I validate that a task record related to the property (from scenario 2), has been successfully created
    cy.get(task_object.Relationship).contains(test_data.PropertyName)

    // And I click on Task dropdown from the left-hand panel
    cy.get(task_object.TaskSubLink).should('be.visible').click()

    // And I validate that the task I have just created is visible on the task index page
    cy.inputText(task_object.SearchField,test_data.TaskDescription)
    cy.get(task_object.SearchButton).should('be.visible').click({ force: true })
    cy.get(task_object.SearchResultDesc,{ timeout: 10000 }).should('be.visible').should('contain',test_data.TaskDescription)

    // Then I validate that a task I have just created is visible on Dashboard Notifications section
    cy.get(home_object.Dashboard).scrollIntoView().should('be.visible').click()
    cy.get(task_object.NotiMessage,{ timeout: 10000 }).should('be.visible')
  })

})
