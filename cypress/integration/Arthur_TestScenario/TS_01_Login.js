/// <reference types="cypress" />

// declare global variables
let test_data
let login_object
let home_object
let property_object

context('Aliasing', () => {
  beforeEach(() => {
    // Given I am a user
    // And I have an existing account on the AUT
    cy.fixture('Staging/test_data').then((variables) => {
      test_data = variables.ARTHUR
    })
    cy.fixture('Staging/Object_Login').then((object) => {
      login_object = object.ARTHUR_LOGIN
    })
    cy.fixture('Staging/Object_Login').then((object) => {
      home_object = object.ARTHUR_HOMEPAGE
    })
    cy.fixture('Staging/Object_Login').then((object) => {
      property_object = object.ARTHUR_ADD_PROPERTY
    })

    // When I open the browser
    cy.visit('https://staging.arthuronline.co.uk/login?X-MODE=QA-eW91LXdpbi1ub3RoaW5nCg')
  })

  it('Login to Arthur website', () => {
    
    // And I proceed to the AUT’s login page
    cy.get('.logo-default').should('be.visible')

    //And I enter the login credentials which have been provided to me in the table above
    cy.get(login_object.Username)
      .type(test_data.Username, { delay: 10 })
      .should('have.value', test_data.Username)

    cy.get(login_object.Password)
      .type(test_data.Password, { delay: 10 })
      .should('have.value', test_data.Password)

    //And I click on the login button
    cy.get('.submit input').click()

    //Then I will be able to login to the AUT
    cy.get('a .account-details').should('be.visible')

  })

})