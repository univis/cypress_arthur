// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
Cypress.Commands.add('inputText', (locator,text) => {
    cy.get(locator).clear().type(text).should('have.value', text)
})

Cypress.Commands.add('SelectUnitSetting', (refcde,field,value) => {
    // cy.get('\/\/input[contains(@name,"[1]")][@id='MultiUnitIdProfileAddressName']//ancestor::tr//td//select[@id='MultiUnitIdUnitOwnerId']')
    cy.xpath('//input[contains(@name,"[1]")][@id=\'MultiUnitIdProfileAddressName\']//ancestor::tr//td//select[@id=\'MultiUnitIdUnitOwnerId\']')
    .type(text).should('have.value', text)
})
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
